<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@dashboard');
Route::get('/register', 'AuthController@daftar');
Route::post('/welcome', 'AuthController@kirim');

Route::get('/table', function(){
    return view('halaman.table');
});

Route::get('/data-table', function(){
    return view('halaman.data-table');
});

// Route::get('/master', function(){
//     return view('layout/master');
// });

//CRUD Cast

//Create
//Form input data create Cast
Route::get('/cast/create', 'CastController@create');
//untuk menyimpan data ke database
Route::post('/cast', 'CastController@store');

//Read
//menampilkan semua data di tabel cast
Route::get('/cast', 'CastController@index');
//menampilkan data berdasarkan id
Route::get('/cast/{cast_id}', 'CastController@show');

//Update
//form edit data dari Cast
Route::get('cast/{cast_id}/edit', 'CastController@edit');
//untuk update data berdasarkan id di table Cast
Route::put('/cast/{cast_id}', 'CastController@update');

//Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');
