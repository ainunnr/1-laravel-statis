@extends('layout.master')
@section('judul')
    Halaman Edit Cast
@endsection
@section('judul1')
    Form Edit Cast
@endsection

@section('konten')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama Cast:</label>
        <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur:</label>
        <input type="number" name="umur" value="{{$cast->umur}}" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio:</label>
        <textarea class="form-control" name="bio" rows="3">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
    
@endsection