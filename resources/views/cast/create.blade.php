@extends('layout.master')
@section('judul')
    Halaman Tambah Cast
@endsection
@section('judul1')
    Tambah Cast
@endsection

@section('konten')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama Cast:</label>
        <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur:</label>
        <input type="number" name="umur" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio:</label>
        <textarea class="form-control" name="bio" rows="3"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
    
@endsection