@extends('layout.master')
@section('judul')
    Halaman Detail Cast
@endsection
@section('judul1')
    Detail Cast
@endsection

@section('konten')

    <h1>{{$cast->nama}}</h1>
    <p>Umur: {{$cast->umur}}</p>
    <p>bio: {{$cast->bio}}</p>

    <a href="/cast" class="btn btn-primary btn-sm my-2">Kembali</a>
    
@endsection