@extends('layout.master')

@section('judul')
    Buat Account Baru
@endsection

@section('judul1')
    Sign Up Form
@endsection

@section('konten')

    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br>
        <input type="text" id="firstname" name="firstname"> <br> <br>

        <label>Last name:</label><br>
        <input type="text" id="lastname" name="lastname"> <br> <br>
        
        <!-- <p>Gender:</p> -->
        <label>Gender: </label> <br>
        <input type="radio" id="male" name="gender" value="Male"><label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="Female"><label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="Male"><label for="other">Other</label>  <br> <br>
        
        <!-- <p>Nationality:</p> -->
        <label>Nationality: </label> <br>
        <select id="nationality" name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select> <br> <br>
        
        <!-- <p>Language Spoken:</p> -->
        <label>Language Spoken:</label> <br>
        <input type="checkbox" id="language1" name="language[]" value="Bahasa Indonesia">
        <label for="vehicle1"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="language2" name="language[]" value="English">
        <label for="vehicle2"> English</label><br>
        <input type="checkbox" id="language3" name="language[]" value="Other">
        <label for="vehicle3"> Other</label><br><br>
        
        <!-- <p>Bio:</p> -->
        <label>Bio:</label> <br>
        <textarea id="bio" name="bio" rows="10" cols="30"></textarea>
        <br>
        
        <input type="submit" value="kirim">
    </form>
    
@endsection
